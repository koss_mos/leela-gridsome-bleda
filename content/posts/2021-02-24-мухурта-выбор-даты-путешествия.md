---
title: Мухурта. Выбор даты путешествия
description: >-
  Путешествием является любое перемещение по какой-либо территории/ акватории с
  любой целью от развлечения до изучения. Исходя из этого, сами понимаете, что
  показателей для путешествия масса, и потому я разделила путешествия на разные
  группы
date: 2021-02-24T12:35:32.088Z
categories:
  - jyotish
tags:
  - Тану
  - Гуру
  - Шукра
  - Мангала
  - Шани
  - Раху
  - Чандра
  - Ревати
  - Дхаништха
  - Шравана
  - Мула
  - Анурадха
  - Хаста
  - Пушья
  - Пунарвасу
  - Мригашира
  - Ашвини
  - Амавасья
  - Пурнима
  - Пурва-бхадрапада
  - Вишакха
  - Пурва-пхалгуни
  - Ашлеша
  - Ардра
  - Криттика
  - Бхарани
  - Меша
  - Вришабха
  - Карка
  - Симха
  - Тула
  - Дхану
  - Аюр
  - Буддх
  - Мокша
  - Карма
  - Лабха
  - Дхарма
  - Калатра
  - Митхун
  - Кумбха
fullscreen: false
---
Путешествием является любое перемещение по какой-либо территории/ акватории с любой целью от развлечения до изучения. Исходя из этого, сами понимаете, что показателей для путешествия масса, и потому я разделила путешествия на разные группы.

Путешествие ВООБЩЕ пройдёт благоприятно и принесёт настоящую радость, если учесть рекомендации ниже.

**Вара (день недели)**

Понедельник не подходит для путешествия на восток, вторник - на север, среда - на север, четверг - на юг, пятница - на запад, суббота - на восток, воскресенье - на запад.

**Титхи (лунный день)**

Наилучшие для начала путешествия: 2, 3, 5, 7, 10, 11, 13, 14 дни, не подходят - 15 (амавасья и пурнима).

**Накшатра (созвездие)**

Наилучшие для начала путешествия, из которого человек вернётся домой быстро и удовлетворённый выполненной работой: Ашвини, Мригашира, Пунарвасу, Пушья, Хаста, Анурадха,  Мула, Шравана, Дхаништха, Ревати; не подходят - Бхарани, Криттика, Ардра, Ашлеша, П.пхалгуни, Вишакха, П.бхадра Лучше для начала путешествия выбрать 2-ю, 3-ю или 4-ю паду (четверть) и избежать 1-ю.

**Лагна (восходящий знак)**

Важное путешествие будет успешным, если выбрать для его начала лагны Меша (овен), Вришабха (телец), Карка (рак), Симха (лев), Тула (весы) или Дхану (стрелец). Лагна с джанма раши максимально благоприятна, но с джанма лагной - неблагоприятна. Также неблагоприятны 5-я, 7-я, 9-я лагны от джанма лагны. Несут успех путешествию хорошо расположенные в лагне Гуру и Шукра.
	

**Важные для путешествий комбинации в гороскопе:**

***Чандра***

* силён и благоприятен на момент начала путешествия,
* в 3-м, 6-м, 9-м или 12-м доме, а Гуру в кендре от лагны,
* в лагне силён, благодаря расположению Гуру или Шукры в кендре,

***Комбинации***

* Сильный Гуру в лагне, а Чандра - вне 8-го дома,
* Поездка будет лёгкой, если Чандра в 7-м, а Шукра и Буддха - в 4-м доме,
* Буддха в 4-м доме и Гуру во 2-м или 7-м нейтрализуют все другие неблагоприятные влияния,
* Гуру в лагне, папа грахи в упачайа и Шукра вне 7-го дома является идеальной комбинацией,
* Благоприятные грахи в кендре и трине.

Следует избегать дней весеннего и осеннего равноденствия и Сурья санкранти.

***Долгое путешествие***

Чандра: силён и благоприятен Лагна: благоприятно, если соответствует джанма раши  Мангал: вне 8-го дома 
Избегать: папа грах в 7-м доме

* поездка на авто или ж/д: избегать поражения лагны и хозяина 8-го дома от Мангала (опасность катастрофы) или Раху (разочарование или болезнь)

***Короткое путешествие***

Чандра и лагна: сильны

* при отсутствии выбирается хора (час) сильнейшей грахи в Тара бале и Чандра бале

***Деловое путешествие (бизнес или командировка)***

Чандра: благоприятно расположен; аспектирует Буддху - даёт процветание и успех,  Избегать положение Чандры в 8-м или 12-м домах, иначе человек заболеет в дороге Лагна: должен прийтись на 10-й дом гороскопа
Избегать папа грах в лагне и 9-м доме
Хозяин 12-го дома: доджен быть благоприятно расположен
Избегать поражения хозяина 12-го дома

* для торговли Буддха: должен быть в Лагне, в 10-м или 11-м домах, не должен получать дришти от папа грах (особенно от Шани): при этом ретроградный Буддха является благоприятным и ускоряет сделку ради благополучия \*\* избегать взаимных дришти между Мангалом, Шани и Раху, которые могут дать внезапные остановки и непреодолимые препятствия в пути

***Духовное путешествие (паломничество)***

Гуру должен быть в лагне или в 9-м доме Избегать тех месяцев, когда Гуру сожжён

***Морское путешествие***

Лагна: карка (рак), желательно с водной грахой Избегать Мангала в лагне Избегать Мангала в 7-м и 8-м домах

* Шукра должен быть хорошо расположен

***Воздушное путешествие***

Чандра: растущий и максимально удалён от Раху Лагна: сильна, в воздушном раши, с хорошо расположенным Гуру Избегать Мангала в лагне
Избегать Мангала в 7-м или 8-м домах  

![Мухурта путешествия](/uploads/мухурта-сайт.png "Выбор мухурты для путешествия")

Естественно, всё это уходит на второй план, когда речь идёт об экстренных случаях: тут мы говорим уже не о благоприятной дате для поездки, а просто выбираем хороший час (хору) и пускаемся в путь.

Если у вас есть лунный календарь, то вы без труда справитесь с выбором наиболее подходящего дня и без моей помощи. На фоне роста внутреннего туризма эта тема может оказаться не просто интересной, а и полезной, чего всем желаю!