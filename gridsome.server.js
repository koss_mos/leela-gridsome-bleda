module.exports = function (api) {
  api.loadSource((actions) => {
    const posts = actions.addCollection('Post');
    const categories = actions.addCollection('Category');

    // makes all ids in the `categories` field reference a `Category`
    posts.addReference('categories', 'Category');

    categories.addNode({
      id: 'my',
      title: 'Мысли вслух',
    });

    categories.addNode({
      id: 'jyotish',
      title: 'Лунная астрология Джйотиш',
    });
  });
};
