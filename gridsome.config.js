/* eslint-disable global-require */

module.exports = {
  siteName: 'Leela',
  siteDescription: 'Leela | Размышления, истории и идеи',
  siteUrl: 'https://leela.space/',
  titleTemplate: '%s | Leela',
  icon: 'src/favicon.png',

  transformers: {
    remark: {
      externalLinksTarget: '_blank',
      externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
      plugins: [
        ['gridsome-plugin-remark-shiki', {
          theme: 'min-light',
        }],
      ],
    },
  },

  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        // baseDir: 'content/posts/**',
        // path: '*.md',
        path: 'content/posts/**/*.md',
        typeName: 'Post',
        refs: {
          tags: {
            typeName: 'Tag',
            create: true,
          },
          categories: {
            typeName: 'Category',
          },
        },
      },
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        cacheTime: 600000, // default
      },
    },
    {
      use: 'gridsome-plugin-rss',
      options: {
        contentTypeName: 'Post',
        feedOptions: {
          title: 'Bleda, a Gridsome blog starter',
          feed_url: 'https://gridsome-starter-bleda.netlify.com/feed.xml',
          site_url: 'https://gridsome-starter-bleda.netlify.com',
        },
        feedItemOptions: node => ({
          title: node.title,
          description: node.description,
          url: `https://gridsome-starter-bleda.netlify.com${node.path}`,
          date: node.date,
        }),
        output: {
          dir: './static',
          name: 'feed.xml',
        },
      },
    },
    {
      use: 'gridsome-plugin-netlify-cms',
      options: {
        publicPath: '/admin',
      },
    },
    {
      use: 'gridsome-plugin-gtm',
      options: {
        id: 'GTM-N3SF52Q',
        enabled: true,
        debug: true,
      },
    },
    {
      use: '@zefman/gridsome-source-instagram',
      options: {
        username: 'leela_dasi_', // Instagram username
        typeName: 'InstagramPhoto', // The GraphQL type you want the photos to be added under. Defaults to InstagramPhoto
      },
    },
    {
      use: 'gridsome-plugin-pug',
      options: {
        pug: { /* Options for `pug-plain-loader` */ },
        pugLoader: { /* Options for `pug-loader` */ },
      },
    },
  ],

  templates: {
    Tag: '/tag/:id',
    Post: '/:title',
    Category: '/category/:id',
  },

  chainWebpack: (config) => {
    config.module
      .rule('css')
      .oneOf('normal')
      .use('postcss-loader')
      .tap((options) => {
        options.plugins.unshift(...[
          require('postcss-import'),
          require('postcss-nested'),
          require('tailwindcss'),
        ]);

        if (process.env.NODE_ENV === 'production') {
          options.plugins.push(...[
            require('@fullhuman/postcss-purgecss')({
              content: [
                'src/assets/**/*.css',
                'src/**/*.vue',
                'src/**/*.js',
              ],
              defaultExtractor: content => content.match(/[\w-/:%]+(?<!:)/g) || [],
              whitelistPatterns: [/shiki/],
            }),
          ]);
        }

        return options;
      });
  },

  css: {
    loaderOptions: {
      // postcss: {
      //   plugins: postcssPlugins,
      // },
      stylus: {
        modules: true,
      },
    },
  },
};
