import DefaultLayout from '~/layouts/Default.vue';

export default function (Vue, { head }) {
  Vue.component('Layout', DefaultLayout);

  head.htmlAttrs = { lang: 'ru', class: 'h-full' };
  head.bodyAttrs = { class: 'antialiased font-serif' };

  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css?family=Fira+Sans:400,700&subset=cyrillic',
  });

  head.meta.push({
    name: "yandex-verification", 
    content: "0230ee463113ac91",
  });
}
